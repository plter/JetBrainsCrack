package com.jetbrains.crack;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by plter on 8/5/15.
 */
public class Main extends Application{

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/com/jetbrains/crack/views/MainForm.fxml"));
        primaryStage.setTitle("JetBrains系列软件注册机");
        primaryStage.setScene(new Scene(root,500,200));
        primaryStage.show();
    }
}
