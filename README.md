#JetBrainsCrack

#这是一个带图形化界面的软件注册机
* 作者博客：[http://plter.com](http://plter.com)
* 内核代码来自：[https://github.com/rover12421/JetbrainsPatchKeygen](https://github.com/rover12421/JetbrainsPatchKeygen)
* 说明：本代码仅用于编程交流，严禁将编译的目标可执行程序传播。如有需要，请购买官方正版软件！